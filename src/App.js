import * as React from "react";
import {
  Scheduler,
  DayView,
  WeekView,
  MonthView,
  SchedulerItem
} from "@progress/kendo-react-scheduler";
import "@progress/kendo-theme-default/dist/all.css";
import { sampleData, displayDate } from "./events-utc.js";

const AdditionalItemPropsContext = React.createContext({});

const CustomItem = props => {
  const additionalProps = React.useContext(AdditionalItemPropsContext);
  return (
    <SchedulerItem
      {...props}
      style={{ ...props.style, color: additionalProps.color }}
    />
  );
};

const App = () => {
  const additionalItemProps = {
    color: "blue"
  };

  return (
    <AdditionalItemPropsContext.Provider value={additionalItemProps}>
      <Scheduler data={sampleData} defaultDate={displayDate}>
        <DayView item={CustomItem} />
        <WeekView item={CustomItem} />
        <MonthView item={CustomItem} />
      </Scheduler>
    </AdditionalItemPropsContext.Provider>
  );
};
export default App;
