## Available Scripts

In the project directory, you can run:
### `npm install`
### `npm start`


## Observations

- By adding Kendo React, the size of the app will increase, and obviously build size will increase. In my case, I was using Fluent-UI and my build size was 2.91 (MB) but there was a scenario where I need to show events so I used the scheduler component of Telerik but this increase builds size from 2.91 (MB) to 4.5 (MB). 
- Telerik (Kendo React) is a framework so it means now I have added two frameworks in my application.
- Telerik is not open source.
- The architecture of Telerik is different. If we want to do some customization then we have to understand the architecture.

### Note

- If we have a scenario where we need to show calendar events then `https://www.npmjs.com/package/react-big-calendar` will be a good choice. 
- It is a stand-alone package.
- By using this my app's build size increase from 2.91 (MB) to 2.96 (MB).
- We can do customization easily as we play with typical react components.
